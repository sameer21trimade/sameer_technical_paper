# Service-Oriented Architecture
## Introduction:
**Service-Oriented Architecture (SOA)** is a methodology of using pre-existing services in your business. Suppose, you want to implement  new technology in your business, then one thing you can do is to start building modules that work on this technology and then integrate these modules in your business. With this kind of approach you will surely fulfill your requirements but in return you and your development team have to invest a lot of time and energy to build these modules. Now, instead of going with this intensive approach we could have followed another approach in which we use a pre-existing product that works on the same technology which we want to implement in our business. This approach takes less time and energy to get our work done, also you do not need to build any module/software from scratch you just need to make use of the already available product.

**SOA** is technically an approach to design software using loose coupling methodology.
The loose coupling concept in SOA is influenced by Object Oriented design, in which the main focus is to minimize the the dependency between 2 modules so that if changes are done in 1 module then it would not affect the functionality of another module.

**SOA** approach allows an application to use multiple services in a network in such a way that all the services linked to this application have minimal dependency between them and if there is any issue with the services then it is the job of service provider to fix that issue without doing any changes in the user application.

#### There are 2 major categories in SOA:

1. **Service provider**: The Service provider is the one who provides the service to the user. It is the job of service provider to deliver their best service to its consumers. In addition, to this the service provider can sign a contract with the consumer which states all the rules and regulations regarding the kind of service that will be provided, how to use it and the fees charged for that service. 

2. **Service consumer**: The Service consumer is the one who will make use of **services** provided by single or multiple service providers. The job of consumer is to collect all the provided services and merge them to make an application work.

![](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-245.png)

### Components of **SOA**

![](https://media.geeksforgeeks.org/wp-content/uploads/Screenshot-248.png)

## Principles of SOA:
1. **Abstraction:** It states that the internal functioning of the services will not be disclosed to the consumer. The services provider hold every right to decide what logic they need to implement to provide the services without informing their consumers. It is responsibility of service provider if there is any glitch or any other issues with their services and in this case they will only be responsible to fix these issues.

1. **Reusability:** It states that the services provided can be re-used to grow the business and increase the productivity. Suppose a business venture is running well and the owner decides to open another branch of this business at some different location then in this case he/she can start the business at new location by just re-using the same reliable services he/she is using currently, there is no need to build up the entire business once again and start from scratch.

1. **Autonomy:** Services provided by service providers are implemented using **Encapsulation** technique in which the user will only get to use theses services but will have no clue of the logic being used to build these services.

1. **Composability:** Using the services provided a user can build an application/software. Service providers provide great support in running these applications thus giving best possible user experience.

1. **Loose coupling:** The services provider and service consumer share a bond called loose coupling in which there is minimal dependency between both of them so that any changes made in services will not affect the quality of user experience.

## Advantages of SOA:
1. **Service Reusability:** In SOA applications make use of pre-existing services, thus services are reused to build many applications.

1. **Easy Maintenance:** Since multiple service providers are used to serve an application also these service providers are not dependent on each other so if there is any issue with any one service provider then this will not harm the functionality of other service providers.

1. **Platform Independent:** Every service provider contributing in service to a user will not have any sort of connection between them they will all serve a unique service to the end user. A user will integrate all these services to build an application.

1. **Availability:** SOA services are easily provided to any consumer on making a request to the service provider.

1. **Reliability:** SOA services are more reliable since it is the service provider responsibility to give best service to its users and things are easy to solve when they are confined to a particular limit as in it is easy to find any error in a single service when compared to fixing bugs in a complex application.

## Disadvantages of SOA:

1. **High Investment:** A huge capital is required to establish a Service-Oriented Architecture.

1. **Complex Service Management:** It is very difficult to handle the situation when services are to be provided to users in huge numbers. In Service Oriented Architecture it is difficult to handle such complicated tasks.

**References:**
1. https://www.geeksforgeeks.org/service-oriented-architecture/

2. https://en.wikipedia.org/wiki/Service_loose_coupling_principle
